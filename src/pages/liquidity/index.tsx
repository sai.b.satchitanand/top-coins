import React, { useContext } from 'react';
import { getLayout } from '../../components/Layout/Layout';
import { ScatterChart, CartesianGrid, XAxis, YAxis, ZAxis, Scatter, Tooltip } from 'recharts';
import { CoinStoreContext } from '../../stores/context';
import { useObserver } from 'mobx-react-lite';
import { currencyFormatter } from '../../utils/formatters';

const Liquidity = (): React.ReactNode => {
  const store = useContext(CoinStoreContext);

  return useObserver(() => {
    if (!store.coins.length) {
      return <span></span>;
    } else {
      return (
        <ScatterChart width={400} height={400} margin={{ top: 20, right: 20, bottom: 20, left: 20 }}>
          <CartesianGrid />
          <ZAxis dataKey={'name'} name="Name" />
          <XAxis dataKey={'x'} type="number" name="Market Cap" unit="$" tickFormatter={currencyFormatter.format} />
          <YAxis dataKey={'y'} type="number" name="Volume (24h)" unit="$" tickFormatter={currencyFormatter.format} />
          <Scatter name="Coins" data={store.scatterData} fill="#8884d8" />
          <Tooltip cursor={{ strokeDasharray: '3 3' }} />
        </ScatterChart>
      );
    }
  });
};

Liquidity.getLayout = getLayout;

export default Liquidity;
