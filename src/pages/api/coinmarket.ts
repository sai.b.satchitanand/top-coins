import { NowRequest, NowResponse } from '@now/node';
import fetch from 'node-fetch';
import querystring from 'querystring';

export default async function (req: NowRequest, res: NowResponse): Promise<void> {
  let results, response;
  try {
    response = await fetch(
      `https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?${querystring.stringify(req.query)}`,
      {
        headers: {
          'X-CMC_PRO_API_KEY': process.env.COINMARKET_KEY,
        },
      },
    );
    results = await response.json();
  } catch (e) {
    res.status(response.status).json(results);
    return;
  }
  res.setHeader('Cache-Control', 's-maxage=10, stale-while-revalidate');
  res.status(response.status).json(results);
}
