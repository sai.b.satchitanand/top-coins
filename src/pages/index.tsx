import React, { useContext } from 'react';
import { AgGridReact } from 'ag-grid-react';
import { ColDef } from 'ag-grid-community';
import { useObserver } from 'mobx-react-lite';
import cx from 'classnames';
import { getLayout } from '../components/Layout/Layout';
import { CoinStoreContext } from '../stores/context';
import { currencyFormatter, percentageFormatter } from '../utils/formatters';
import { Coins } from '../types/coinmarket';
import * as styles from '../components/Market.treat';

const columnDefs: ColDef[] = [
  { headerName: 'Rank', field: 'cmc_rank' },
  { headerName: 'Name', field: 'name' },
  {
    headerName: 'Price',
    cellRenderer({ data }: { data: Coins }): string {
      return currencyFormatter.format(data.quote.USD.price);
    },
  },
  {
    headerName: 'Price Change(24h)',
    cellRenderer({ data }: { data: Coins }): string {
      return `${percentageFormatter.format(data.quote.USD.percent_change_24h)}%`;
    },
  },
  {
    headerName: 'Market Cap',
    cellRenderer({ data }: { data: Coins }): string {
      return `${currencyFormatter.format(data.quote.USD.market_cap)}`;
    },
  },
  {
    headerName: 'Volume (24h)',
    cellRenderer({ data }: { data: Coins }): string {
      return `${currencyFormatter.format(data.quote.USD.volume_24h)}`;
    },
  },
];
const Home = (): React.ReactNode => {
  const store = useContext(CoinStoreContext);

  return useObserver(() => (
    <div className={cx(styles.tableContainerStyle, 'ag-theme-alpine')}>
      <AgGridReact columnDefs={columnDefs} rowData={store.coins} defaultColDef={{ resizable: true }}></AgGridReact>
    </div>
  ));
};

Home.getLayout = getLayout;

export default Home;
