import React, { ReactNode } from 'react';
import { AppProps } from 'next/app';

import 'normalize.css';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';

type AppComponentType = AppProps['Component'] & {
  getLayout?: (a: ReactNode) => ReactNode;
};
type MyAppProps = AppProps & {
  Component: AppComponentType;
};

const MyApp = ({ Component, pageProps }: MyAppProps): ReactNode => {
  const getLayout = Component.getLayout || ((page): ReactNode => page);

  return getLayout(<Component {...pageProps}></Component>);
};

export default MyApp;
