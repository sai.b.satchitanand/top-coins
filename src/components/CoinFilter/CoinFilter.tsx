import React, { useContext } from 'react';
import Select from 'react-select';
import { CoinStoreContext } from '../../stores/context';

const options = [
  { value: 10, label: '10' },
  { value: 50, label: '50' },
  { value: 5000, label: 'all' },
] as const;

type CoinFilterOption = typeof options[number];

export const CoinFilter: React.FC = () => {
  const coinStore = useContext(CoinStoreContext);
  return (
    <Select
      instanceId="coin-filter"
      options={options}
      onChange={(selectedCount): void => {
        coinStore.setFilter(((selectedCount as unknown) as CoinFilterOption).value);
      }}
    ></Select>
  );
};
