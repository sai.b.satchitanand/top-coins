import React, { PropsWithChildren } from 'react';

import { Navigation } from '../Navigation/Navigation';
import * as styles from './Layout.treat';
import { CoinFilter } from '../CoinFilter/CoinFilter';
import { CoinStoreContext } from '../../stores/context';
import { CoinStore } from '../../stores/CoinStore';

const Layout: React.FC = ({ children }: PropsWithChildren<{}>) => {
  return (
    <CoinStoreContext.Provider value={CoinStore.getInstance()}>
      <Navigation />
      <div className={styles.layoutStyle}>
        Select the number of coins
        <span className={styles.filterStyle}>
          <CoinFilter />
        </span>
        {children}
      </div>
    </CoinStoreContext.Provider>
  );
};

export const getLayout: React.FC = (page: React.ReactNode) => <Layout>{page}</Layout>;

export default Layout;
