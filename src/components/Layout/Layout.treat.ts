import { style } from 'treat';

export const layoutStyle = style({
  maxWidth: '1200px',
  width: '100%',
  margin: '0 auto',
  padding: '16px',
  boxSizing: 'border-box',
});

export const filterStyle = style({
  width: '120px',
  display: 'inline-block',
  paddingLeft: '8px',
});
