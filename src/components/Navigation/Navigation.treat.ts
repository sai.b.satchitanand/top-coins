import { style, globalStyle } from 'treat';

export const listStyle = style({
  listStyle: 'none',
  display: 'flex',
  backgroundColor: '#f3f3f3',
  padding: 0,
  margin: 0,
  overflow: 'hidden',
});

globalStyle(`${listStyle} a`, {
  display: 'block',
  textDecoration: 'none',
  color: '#666',
  padding: '8px 16px',
});

globalStyle(`${listStyle} .active a`, {
  color: 'white',
  backgroundColor: '#212529',
});
globalStyle(`${listStyle} li:not(.active) a:hover`, {
  backgroundColor: '#ddd',
});
