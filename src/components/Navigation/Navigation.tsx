import Link from 'next/link';
import React from 'react';
import { useRouter } from 'next/router';
import cx from 'classnames';

import * as styles from './Navigation.treat';

export const RouteUrls = {
  HOME: {
    path: '/',
    title: 'Market overview',
  },
  LIQUIDITY: {
    path: '/liquidity',
    title: 'Liquidity',
  },
} as const;

type RouteUrls = typeof RouteUrls;
type RouteUrl = RouteUrls[keyof RouteUrls];

export const Navigation: React.FC = () => {
  const router = useRouter();
  const listItem = ({ path, title }: RouteUrl): React.ReactNode => {
    return (
      <li
        className={cx({
          active: router.pathname === path,
        })}
        key={path}
      >
        <Link href={path}>{title}</Link>
      </li>
    );
  };

  return (
    <header>
      <nav>
        <ul className={styles.listStyle}>{Object.values(RouteUrls).map(listItem)}</ul>
      </nav>
    </header>
  );
};
