import React from 'react';
import { CoinStore } from './CoinStore';

export const CoinStoreContext = React.createContext<CoinStore>(CoinStore.getInstance());
