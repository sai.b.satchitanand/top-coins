import { observable, action, reaction, computed } from 'mobx';
import { CoinsResponse, Coins } from './../types/coinmarket';

import isServer from '../utils/isServer';

type ScatterData = {
  x: number;
  y: number;
  z: number;
};
export class CoinStore {
  private static instance: CoinStore;

  @observable coins: Coins[] = [];
  @observable numberOfCoins: number;

  constructor() {
    reaction(
      () => this.numberOfCoins,
      () => {
        this.findCoins().then((coins: Coins[]) => {
          this.updateCoins(coins);
        });
      },
    );
  }

  @computed
  get scatterData(): ScatterData[] {
    return this.coins.map((coin) => ({
      x: coin.quote.USD.market_cap,
      y: coin.quote.USD.volume_24h,
      z: coin.quote.USD.percent_change_24h,
      name: coin.name,
    }));
  }

  @action
  updateCoins(coins: Coins[]): void {
    this.coins = coins;
  }

  @action
  setFilter(val: number): void {
    this.numberOfCoins = val;
  }

  private findCoins(): Promise<Coins[]> {
    const query = this.numberOfCoins ? `limit=${this.numberOfCoins}` : '';
    return fetch(`/api/coinmarket?${query}`)
      .then((res) => res.json())
      .then((res: CoinsResponse) => res.data);
  }

  static getInstance(): CoinStore {
    if (isServer() || this.instance === undefined) {
      return (this.instance = new this());
    }
    return this.instance;
  }
}
