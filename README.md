# Top coins

## Demo

[https://top-coins.saibs.now.sh/](https://top-coins.saibs.now.sh/)

Pre-requisite:

- nodejs

Installation instructions:

```sh
npm install
```

Following environment variable is required
COINMARKET_KEY

For development

```sh
npm run dev
```

or to run application in production

```sh
npm run build
npm run start
```

Primary technologies used:

- [React](https://reactjs.org/)
- [Typescript](https://www.typescriptlang.org/)
- [eslint](https://eslint.org/blog/2019/01/future-typescript-eslint)
- [Nextjs](http://nextjs.org) - SSR framework for react, out of the box routing and easy customization.
- [mobx](https://mobx.js.org/) - State management.
- [commitzen](http://commitizen.github.io/cz-cli/) Standardizing commit messages.
